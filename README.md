Account Role
============

This is a role for ansible-galaxy for URL based roles

Requirements
------------


Role Variables
--------------

These variables are external to the role, but used by the role:

Variable                                   | Desc
------------------------------------------ | --------------
hosting_username                           | Name of the user hosting the application
hosting_user_home                          | Home directory of the user hosting the application
account_server_external_host               | Host of the application
account_server_external_port               | Port of the application
account_project_version                    | Version of the app
use_secure_http                            | Should the inbound requests use HTTPS when calling the services?


Dependencies
------------


Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

Apache2

Author Information
------------------

